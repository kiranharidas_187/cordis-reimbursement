import { Injectable } from '@angular/core';
import { CanActivate,Router } from '@angular/router';
import { CurrentUserService } from '../Shared/Service/current-user.service';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private currentUser: CurrentUserService  , private router: Router
  ){}
  canActivate() : boolean{
   if (this.currentUser.loggedIn()){
     return true;
   }
   else{
     this.router.navigate(['/login'])
     return false;
   }
  }
}
