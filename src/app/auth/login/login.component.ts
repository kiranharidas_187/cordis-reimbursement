import { Component, OnInit } from '@angular/core';
import {  FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
// import { error } from '@angular/compiler/src/util';
import {MatSnackBar} from '@angular/material';
// import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { CurrentUserService } from '../../Shared/Service/current-user.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm :FormGroup;
  loginClick :boolean = false; 
  msg:any;
  data1 :any;
  user:any;
  submit(){
   console.log(this.loginForm.value);
   
   this.http.post('http://192.168.1.41:8080/pushcord-security/api/v1.0/jwt-auth/login',this.loginForm.value).pipe(catchError(this.errhand))  
   .subscribe(
    (data:any) => {

      console.log(data),
      this.data1 = JSON.stringify(data.status);   
      if(data.message == null){
        this.currentUser.setCurrentUser(data);
      
        this.currentUser.getCurrentUser();      
        console.log("token: " + this.currentUser.getCurrentToken()); 
        this.router.navigate(['/fileList']);

          
      }else{
      this.openSnackBar(data.message); 
      this.loginClick=false;
      }
    }, 
    error =>{ 
      this.msg = error 
    }
   );
  }
  openSnackBar(x) { 
    this.snackBar.open(x,'Got It!');
  }
  
  
  errhand(error: HttpErrorResponse){ 
   
   return observableThrowError(error.message || "Server Error");
  }

  constructor(private fb: FormBuilder,private http:HttpClient, private snackBar: MatSnackBar , private currentUser: CurrentUserService , private router:Router) { 
    
    this.createForm();
  }

  createForm() {
    this.loginForm = this.fb.group({
      userName: ['',  Validators.compose(
        [Validators.minLength(5), Validators.required])], 
      password:['',  Validators.compose(
        [Validators.minLength(5), Validators.required])] ,
      rememberMe : true,  
    });
  }
 

  ngOnInit() {
  }

}
