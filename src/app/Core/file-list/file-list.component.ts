import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss']
})
export class FileListComponent implements OnInit {



  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.http.post('http://192.168.1.41:8080/pushcord-web/api/v1.0/ui/front-details/list-all-relations',{
      "firstPartyEntityId":1,
      "firstPartyEntityType":"INDIVIDUAL",
      "secondPartyEntityId":1,
      "secondPartyEntityType":"BUSINESS"
    })
    .subscribe(
      (data:any) =>{
        console.log(data);
      }
    )
  }

}
