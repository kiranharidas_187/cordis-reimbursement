import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { FileListComponent } from './Core/file-list/file-list.component';
import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  { 
    path:'login',
    component:LoginComponent,
    
  },
  {
    path: 'fileList',
    component: FileListComponent,
    canActivate:[AuthGuard]
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
