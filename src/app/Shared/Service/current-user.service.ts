import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class CurrentUserService {
  currentToken:any;
  jsonInfo:any;
  constructor( ) { }
  
  public setCurrentUser(userInfo : any){    
    userInfo = JSON.stringify(userInfo);
    userInfo = window.btoa(userInfo);   
    localStorage.setItem('Info', userInfo);
  }
  public getCurrentUser(){
    this.jsonInfo= localStorage.getItem('Info');
    this.jsonInfo  = window.atob(this.jsonInfo);
    this.jsonInfo =JSON.parse( this.jsonInfo);
    console.log('Decrypted Info',this.jsonInfo);
    return this.jsonInfo;  
    
  }
  public deleteCurrentUser(){
    localStorage.removeItem('Info');
  }
 
  public getCurrentToken(){

    //  let token =  this.jsonInfo['object']['signedToken'];
     return this.jsonInfo['object']['signedToken'];
  }
  public loggedIn(){
    return !! localStorage.getItem('Info');
    
  }

  public currentVCA(){
    return {
      "entityId":1,
      // "name":"Nilkamal Limited",
      "frontType":"BUSINESS",
      // "frontId":"57941",
      // "currentIconPath":"/pushcord-images/BUSINESS/1_1529074368063.jpg",
      // "cfgGenericRelationShipId":4,
      // "relationshipName":"Employee",
      // "customRelationshipName":null,
      // "connectionCordName":null,
      // "ccId":1,
      // "uccId":1
      }

  }


}
