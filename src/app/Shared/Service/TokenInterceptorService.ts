import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { CurrentUserService } from './current-user.service';



@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {
  constructor(private injector:Injector) { }

  intercept(req , next){
    console.log('redquest',req.url);
   
    let currentUser = this.injector.get(CurrentUserService)
    let tokinzedReq = req.clone({          
        setHeaders :{
          Authorization: `Bearer ` 
        }
      })
      if(req.url.indexOf("login")){
        return next.handle(tokinzedReq)
      }
      else{
        return null
      }
     
  }
}