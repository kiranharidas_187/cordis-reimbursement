import { Component, OnInit } from '@angular/core';
import { CurrentUserService } from '../../Service/current-user.service';


@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {

  // logoutClick: boolean= false;
  constructor(private currentUserService:CurrentUserService) { }
  // clickDots(){
  //   if(this.logoutClick==false)
  //   this.logoutClick=true;
  //   else
  //   this.logoutClick=false;
  // }
  logout(){
    this.currentUserService.deleteCurrentUser();
  }
  ngOnInit() {
  }

}
